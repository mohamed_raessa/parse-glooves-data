#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <string>
#include <set>
#include <exception>
#include <iostream>
namespace pt = boost::property_tree;

// function to print boost ptree 
std::string indent(int level)
{
    std::string s;
    for (int i = 0; i < level; i++)
        s += "  ";
    return s;
}

void printTree(boost::property_tree::ptree &pt, int level)
{
    if (pt.empty())
    {
        std::cout << "\"" << pt.data() << "\"";
    }

    else
    {
        if (level)
            std::cout << std::endl;

        std::cout << indent(level) << "{" << std::endl;

        for (boost::property_tree::ptree::iterator pos = pt.begin(); pos != pt.end();)
        {
            std::cout << indent(level + 1) << "\"" << pos->first << "\": ";

            printTree(pos->second, level + 1);
            ++pos;
            if (pos != pt.end())
            {
                std::cout << ",";
            }
            std::cout << std::endl;
        }

        std::cout << indent(level) << " }";
    }
    std::cout << std::endl;
    return;
}
//


// transorm data
struct Transform
{
    std::string frame_id;
    std::string child_frame_id;
    std::vector<double> translation{0, 0, 0.0, 0.0};
    std::vector<double> rotation{0.0, 0.0, 0.0, 0.0};
};

//glove data
struct GloveData
{
    std::string op;
    std::string topic;
    std::vector<Transform> transforms;

    void load(const std::string &filename);
};

void GloveData::load(const std::string &filename)
{
    // create empty property tree
    pt::ptree tree;

    // parse the JSON into the property tree.
    pt::read_json(filename, tree);

    // fill the glove data values from the ptree
    op = tree.get<std::string>("op");
    topic = tree.get<std::string>("topic");

    std::cout << op << " - " << topic << std::endl;

    // fill the transforms from the msg.transforms modules in the JSON file
    // get_child finds the node containing modules, and iterate over
    // its children. If the path cannot be resolved, get_child throws.
    BOOST_FOREACH (pt::ptree::value_type &mod_element, tree.get_child("msg.transforms"))
    {
        Transform tf;

        tf.frame_id = mod_element.second.get<std::string>("header.frame_id");
        tf.child_frame_id = mod_element.second.get<std::string>("child_frame_id");

        tf.translation[0] = mod_element.second.get<double>("transform.translation.x");
        tf.translation[1] = mod_element.second.get<double>("transform.translation.y");
        tf.translation[2] = mod_element.second.get<double>("transform.translation.z");

        tf.rotation[0] = mod_element.second.get<double>("transform.rotation.x");
        tf.rotation[1] = mod_element.second.get<double>("transform.rotation.y");
        tf.rotation[2] = mod_element.second.get<double>("transform.rotation.z");
        tf.rotation[3] = mod_element.second.get<double>("transform.rotation.w");

        transforms.emplace_back(tf);

        std::cout << "Frame id " << tf.frame_id << " - Child frame id" << tf.child_frame_id << std::endl;
        std::cout << "Translation " << tf.translation[0] << tf.translation[0] << tf.translation[0] << std::endl;
        std::cout << "Rotation " << tf.translation[0] << tf.translation[0] << tf.translation[0] << "\n"
                  << std::endl;
    }
}

int main()
{
    try
    {
        GloveData gd;
        gd.load("../glove_ros_data.json");
        std::cout << "Success\n";
    }
    catch (std::exception &e)
    {
        std::cout << "Error: " << e.what() << "\n";
    }
    return 0;
}
